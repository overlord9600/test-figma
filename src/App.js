import './App.css';
import Header from './parts/Header';
import { BrowserRouter, Route } from 'react-router-dom';
import Main from './components/mainPage/main'
import AnaliticBlock from './parts/AnaliticBlock';
import Availability from './components/availabilityPage/availability';
import ProfitPage from './components/profitPage/Profit';
import Expense from './components/expensePage/Expense';
import Banks from './components/banksPage/Banks';
import Fund from './components/FundPage/Fund';
import Invest from './components/InvestPage/Invest';
import Arrearage from './components/arrearagePage/Arrearage';
import OrgonaizerPage from './components/orgonaizer/OrgonaizerPage';
function App() {
  return (
     <div className='all'>
       <div className='page'>
           <Header/>
           <AnaliticBlock/>
           <BrowserRouter>
            <Route path='/' exact component={Main}/>
            <Route path='/availability' component={Availability}/>
            <Route path='/profit' component={ProfitPage}/>
            <Route path='/expense' component={Expense}/>
            <Route path='/banks' component={Banks}/>
            <Route path='/fund' component={Fund}/>
            <Route path='/invest' component={Invest}/>
            <Route path='/arrearage' component={Arrearage}/>
            <Route path='/orgonaizer' component={OrgonaizerPage}/>
           </BrowserRouter>
       </div>
     </div>
  );
}

export default App;
