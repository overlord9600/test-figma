import dollar from '../../components/images/cashMoney.jpg'
import coins from '../../components/images/coins.jpg'
import outgoing from '../../components/images/dplain.jpg'
import bank from '../../components/images/banks.jpg'
import property from '../../components/images/gold.jpg'
import invest from '../../components/images/invest.jpg'
import debt from '../../components/images/debt.jpg'
import sell from '../../components/images/icon/sellIcon.png'
import organizer from '../../components/images/organiser.jpg'

export default{
    navRow1:[
        {id:'/availability',img:dollar,title:'НАЛИЧИЕ',currency:'0.00 USD',ok:'0.000',route:'availability',show:false},
        {id:'/profit',img:coins,title:'доходы',currency:'0.00 USD',ok:'0.000',route:'profit',show:false},
        {id:'/expense',img:outgoing,title:'расходы',currency:'0.00 USD',ok:'0.000',route:'expense',show:false},
        {id:'/banks',img:bank,title:'банки',currency:'0.00 USD',ok:'0.000',route:'banks',show:false},
        {id:'/fund',img:property,title:'капитал',currency:'0.00 USD',ok:'0.000',route:'fund',show:false},
        {id:'/invest',img:invest,title:'инвестиция',currency:'0.00 USD',ok:'0.000',route:'invest',show:false},
        {id:'/arrearage',img:debt,title:'долги',currency:`0.00 USD`,ok:'0.000',route:'arrearage',show:false},
        {id:'/orgonaizer',img:organizer,title:'органайзер',currency:'0.00 USD',ok:'0.000',route:'orgonaizer',show:false},
    ],
    navRow2:[],
    currentPage:[],
    historyContent:false,
    banks:[
        {text:'0.00 USD',price:'YOUR TEXT'},
        {text:'0.00 USD',price:'YOUR TEXT'},
        {text:'0.00 USD',price:'YOUR TEXT'},
        {text:'0.00 USD',price:'YOUR TEXT'},
    ],
    fundTable:[
        {currency:'usd', cost:'20usd', descrip:'name',category:'name',sell:sell},
        {currency:'usd', cost:'20usd', descrip:'name',category:'name',sell:sell},
        {currency:'usd', cost:'20usd', descrip:'name',category:'name',sell:sell},
        {currency:'usd', cost:'20usd', descrip:'name',category:'name',sell:sell},
    ],
    categories:[],
    profitCategories:[],
    categoryModalOpen:false,
    categoryModalOpen2:false,
    currentObject:[],
    addCategoryValue:{
        category:'',
        currency:'',
        descrip:'',
    },
    profitCategoryValue:{
        currency:'',
        descrip:'',
        descrip:'',
    },
    profitCategoryValue2:{
        currency:'',
        descrip:'',
    },
    addCategoryValue2:{
        currency:'',
        descrip:'',
    },
    historyTable:[],
    addCategoryValueError:''
}