import navs from '../states/navState'

export default function navReducer(state=navs,action){
    const temp = {...state}
        if(action.type==='clickCard'){            
            temp.navRow1[action.index].show=true
            return temp
        }

        if(action.type==='mouseIsOut'){
            temp.navRow1[action.index].show=false
            return temp
        }

        
        if(action.type==='currentPage'){
             temp.navRow1.filter(item=>{
                 if(item.id===action.location){
                    return temp.currentPage=item
                 }
             })
              let a=[]
             temp.navRow1.filter((item)=>{
                if(item.id!==action.location){
                   return a.push(item)       
                } 
            })
            temp.navRow2=a
             return temp
        }

        if(action.type==='showHistory'){
            if(temp.historyContent==false){
                temp.historyContent=true
            }else{
                temp.historyContent=false
            }

            return temp
        }

        if(action.type==='createNewCategory'){
            if(temp.categoryModalOpen==false){
                temp.categoryModalOpen=true
            }else{
                temp.categoryModalOpen=false
            }
        }

        if(action.type==='changeNewCategory'){
            temp.addCategoryValue[action.key]=action.value
            return temp
        }
        if(action.type==='changeNewCategory2'){
            temp.addCategoryValue2[action.key]=action.value
            return temp
        }

        if(action.type==='changeNewProfit'){
            temp.addCategoryValue2[action.key]=action.value
            return temp
        }

        

        if(action.type==='pushNewCategory'){
            console.log(temp.addCategoryValue);
             if(isNaN(temp.addCategoryValue.currency)){
                 temp.addCategoryValueError='Currency must be a number'
             }else if(temp.addCategoryValue.currency===''||temp.addCategoryValue.descrip===''||temp.addCategoryValue.category===''){
                temp.addCategoryValueError='Fill in all inputs'
             }else if(temp.addCategoryValue.currency!==''&&temp.addCategoryValue.descrip!==''&&temp.addCategoryValue.category!==''){
                temp.addCategoryValueError=''
                 temp.categories.push(temp.addCategoryValue);
                 temp.historyTable.push(temp.addCategoryValue);
             }
             return temp        
        }

        if(action.type==='openToAdd'){
            temp.categoryModalOpen2=true
             
            localStorage.setItem('category', action.obj.category)
            return temp
        }

    

        if(action.type==='addToCategory'){
            if(isNaN(temp.addCategoryValue2.currency)){
                temp.addCategoryValueError='something went wrong'
             }else if(!temp.addCategoryValue2.currency||!temp.addCategoryValue2.descrip){
                temp.addCategoryValueError='Fill in all inputs'
             }else{
                temp.historyTable.push({category:localStorage.category,currency:temp.addCategoryValue2.currency,descrip:temp.addCategoryValue2.descrip});
                console.log(temp.historyTable);
             }
             return temp
        }


if(action.type==='addToProfitCategory'){
    if(isNaN(temp.addCategoryValue2.currency)){
        temp.addCategoryValueError='something went wrong'
     }else if(!temp.addCategoryValue2.currency||!temp.addCategoryValue2.descrip){
        temp.addCategoryValueError='Fill in all inputs'
     }else{
        temp.historyTable.push({category:localStorage.category,currency:temp.addCategoryValue2.currency,descrip:temp.addCategoryValue2.descrip});
        console.log(temp.historyTable);
     }
     return temp
}

        if(action.type==='closeModal'){
            temp.categoryModalOpen=false
            return temp
        }

        if(action.type==='closeModal2'){
            localStorage.clear()
            temp.categoryModalOpen2=false
            return temp
        }

        if(action.type==='createNewCategory2'){
            if(temp.categoryModalOpen2==false){
                temp.categoryModalOpen2=true
            }else{
                temp.categoryModalOpen2=false
            }
        }

    return temp
}