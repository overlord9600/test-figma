import navs from './navReducer'
import {combineReducers} from 'redux'

let root = combineReducers({navs})

export default root