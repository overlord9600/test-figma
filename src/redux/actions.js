export function clickCard(index){
    return{
        type:'clickCard',
        index
    }
}

export function mouseIsOut(index){
    return{
        type:'mouseIsOut',
        index
    }
}

export function currentPage(location){
    return{
        type:'currentPage',
        location
    }
}

export function goTo(item,index,history){
    return{
        type:'goto',
        item,
        index,
        history
    }
}

export function showHistory(){
    return{
        type:'showHistory'
    }
}

export function openToAdd(obj){
    return{
        type:'openToAdd',
        obj
    }
}

export function createNewCategory(){
    return{
        type:'createNewCategory'
    }
}

export function createNewProfit(){
    return{
        type:'createNewProfit'
    }
}


export function changeNewCategory(key,value){
    return{
        type:'changeNewCategory',
        key,
        value
    }
}

export function changeNewProfit(key,value){
    return{
        type:'changeNewProfit',
        key,
        value
    }
}

export function changeNewCategory2(key,value){
    return{
        type:'changeNewCategory2',
        key,
        value
    }
}

export function changeNewProfitCategory(key,value){
    return{
        type:'changeNewProfitCategory',
        key,
        value
    }
}

export function pushNewCategory(){
    return{
        type:'pushNewCategory'
    }
}

export function pushNewCategory2(){
    return{
        type:'pushNewCategory'
    }
}



export function addToCategory(){
    return{
        type:'addToCategory'
    }
}

export function addToProfitCategory(){
    return{
        type:'addToProfitCategory'
    }
}

export function closeModal(){
    return{
        type:'closeModal'
    }
}

export function closeModal2(){
    return{
        type:'closeModal2'
    }
}


//profit page
export function pushNewProfitCategory(){
    return{
        type:'pushNewProfitCategory'
    }
}