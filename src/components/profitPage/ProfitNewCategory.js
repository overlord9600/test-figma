import React from 'react';
import { connect } from 'react-redux';
import { changeNewCategory, changeNewProfitCategory, closeModal, pushNewCategory, pushNewProfitCategory } from '../../redux/actions';
const ProfitNewCategory = (props) => {
    return (
        <div className={props.navs.categoryModalOpen==true?'bgLayer':'closedModal'}>
            <div className='modal'>
                <div className='modalHead'>
                    <div className='modalTitle'><p>Приход</p> {props.navs.addCategoryValueError}</div>
                    <div className='closeModal' onClick={()=>{props.dispatch(closeModal())}}>&times;</div>
                </div>
                <div className='modalContent'>
                    <div className='leftSide'>
                        <div className='textfeiled'>
                       Категория
                                <input 
                                value={props.navs.addCategoryValue.category} 
                                onChange={(e)=>{props.dispatch(changeNewProfitCategory('category',e.target.value))}}
                                />
                        
                        </div>
                        <div className='textfeiled'>
                            Сумма
                                <input 
                                // value={props.navs.addCategoryValue.currency} 
                                // onChange={(e)=>{props.dispatch(changeNewProfitCategory('currency',e.target.value))}}
                                />
                          
                        </div>
                        <div className='textfeiled'>
                              Описание
                            <textarea 
                            // value={props.navs.addCategoryValue.descrip} 
                            // onChange={(e)=>{props.dispatch(changeNewProfitCategory('descrip',e.target.value))}}
                            ></textarea>
                            
                        </div>
                        
                       
                    </div>
                    <div className='rightSide'>
                        <div className='file'>
                            <label  htmlFor='file' >
                                <input id='file' type='file'/>
                            </label>
                        </div>
                        <div className='flagIcons'>

                        </div>
                        <div className='btn'>
                            <button className='add' 
                            onClick={()=>props.dispatch(pushNewProfitCategory())}
                            >Добавить</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
 
 
 
export default connect(r=>r)(ProfitNewCategory);