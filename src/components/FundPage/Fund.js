import React, { useEffect } from 'react';
import SmallCards from '../../parts/smallCards';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { currentPage } from '../../redux/actions';
import { connect } from 'react-redux';
import { IconButton } from '@material-ui/core';
import dots from '../images/icon/dots.png'
import lastIcon from '../images/icon/lastIcon.png'
import edit from '../images/icon/editIcon.png'
import remove from '../images/icon/clearIcon.png'
import moreCoins from '../images/moreCoins.png'
import './fund.css'
function Fund (props){
    useEffect(()=>{
        props.dispatch(currentPage(props.history.location.pathname))        
    },[])

    let item = props.navs.currentPage
    return(
        <div className='fundPage'>
        <SmallCards/>
        <div className='content'>
            <div className='goBack'>
                <a href='/'>
                    <ArrowBackIcon/>
                </a>
                <h3>{item.title}</h3>      
            </div>
            <div className='icons'>
             

                
                </div>             
        </div>
        <div className='description'>
            <div className='largeCard'>
               <div className='largeCardContent'>
               <div className='cardImage'>
                    <img src={moreCoins}/>
                </div>
                <div className='largeCardInfo'>
                    <span>описание</span>
                    <span>0.00 USD</span>
                </div>
               </div>
                <div className='cardIcon'>
                    <IconButton>
                        <img src={dots}/>
                    </IconButton>
                </div>
            </div>
            <div className='lastIcon'>
                <IconButton>
                    <img src={lastIcon} />
                </IconButton>
            </div>
            
        </div>
        <div className='selectBlock'>
                <select>
                    <option>категории</option>
                </select>
            </div>
            <div className='tableBlock'>
            <table>
                <thead>
                <tr>
                    <th>Валюта</th>
                    <th>Стоимость</th>
                    <th>Описание</th>
                    <th>Категория</th>
                    <th>Продать</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    props.navs.fundTable.map(item=>{
                        return(
                            <tr>
                                <td>{item.currency}</td>
                                <td>{item.cost}</td>
                                <td>{item.descrip}</td>
                                <td>{item.category}</td>
                                <td><div className='sellblock'><img src={item.sell}/></div></td>
                                <td>
                                    <IconButton><img src={edit} /></IconButton>
                                    <IconButton><img src={remove} /></IconButton>
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>

            </div>

        </div>

    )

}

export default connect(r=>r)(Fund)