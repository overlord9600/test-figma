import React, { useEffect } from 'react';
import SmallCards from '../../parts/smallCards';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { currentPage } from '../../redux/actions';
import { connect } from 'react-redux';
import addbank from '../images/icon/addBank.png'
import dots from '../images/icon/whitedote.png'

import './bank.css'
function Banks (props){
    useEffect(()=>{
        props.dispatch(currentPage(props.history.location.pathname))        
    },[])
    let item = props.navs.currentPage
    return(
        <div className='bankPage'>
        <SmallCards/>
        <div className='content'>
            <div className='goBack'>
                <a href='/'>
                    <ArrowBackIcon/>
                </a>
                <h3>{item.title}</h3>
            </div>
            <div className='icons'>
             
                
                </div>             
        </div>
        <div className='mainContent'>
            <div className='addBankIcon'>
                <img src={addbank}/>
            </div>
                {
                    props.navs.banks.map(items=>{
                        return(
                            <div className='bankcard'>
                                <img className='bankImg' src={item.img}/>
                                <div className='caption'>
                                    <img className='dote' src={dots}/>
                                    <div>
                                        <span>{items.text}</span>
                                        <span>{items.price}</span>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }          
            </div>
        

        </div>

    )

}

export default connect(r=>r)(Banks)