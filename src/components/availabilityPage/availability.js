import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import SmallCards from '../../parts/smallCards';
import { clickCard, currentPage, mouseIsOut, showHistory } from '../../redux/actions';
import './availability.css'
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Link } from 'react-router-dom';
import { IconButton } from '@material-ui/core';
import HoverIcon from '../../parts/hoverIcon'
import History from '../images/icon/history.png'
import his1 from '../images/icon/his1.png'
import his2 from '../images/icon/his2.png'
import his3 from '../images/icon/his3.png'
import his4 from '../images/icon/his4.png'
import dots from '../images/icon/dots.png'
import lastIcon from '../images/icon/lastIcon.png'
import moreCoins from '../images/moreCoins.png'
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Contents from '../../parts/contents';
import ContentHistory from '../../parts/ContentHistory';
import NewCategory from '../../parts/createNewCategory/newCategory';
import AddCategory from '../../parts/createNewCategory/addCategory';


const useStylesBootstrap = makeStyles((theme) => ({
    arrow: {
      color: '#FFFFFF',
    },
    tooltip: {
        backgroundColor: '#FFFFFF',
        color: 'rgba(0, 0, 0, 0.87)',
        maxWidth: 220,
        fontSize: '14px',
        border: '1px solid #dadde9',
        borderRadius:'10px',
        lineHeight:'16.41px',
        boxShadow: '2px 3px 10px 3px #E5E5E5',
        textTransform:'uppercase',
        width:'250px',
        height:'32px',
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
    },
    
  }));


  function BootstrapTooltip(props) {
    const classes = useStylesBootstrap();
  
    return <Tooltip arrow classes={classes} {...props} />;
  }

function AvailabilityPage (props){
    
     useEffect(()=>{
        props.dispatch(currentPage(props.history.location.pathname))        
    },[])
let item = props.navs.currentPage
    return(
        <div className='availabilityPage'>
            <NewCategory/>
            <AddCategory/>
            <SmallCards/>
        <div className='content'>
            <div className='goBack'>
                <Link to='/'>
                    <ArrowBackIcon/>
                </Link>
                <h3>{props.navs.historyContent==false?item.title:'наличные деньги-история'}</h3>        
            </div>
            <div className='icons'>

            <BootstrapTooltip title="наличные деньги-история ">
                <div>
                    <IconButton onClick={()=>props.dispatch(showHistory())}>
                        <img src={History}/>
                    </IconButton>
                </div>
            </BootstrapTooltip>

                <div>
                    <IconButton>
                        <img src={his1}/>
                    </IconButton>
                </div>

                <div>
                <IconButton>
                    <img src={his2}/>
                </IconButton>
                </div>

                <div>
                    <IconButton>
                        <img src={his3}/>
                    </IconButton>
                </div>

                <div>
                    <IconButton>
                        <img src={his4}/>
                    </IconButton>
                </div>  

            </div>   

        </div>
          {
              props.navs.historyContent==false?<Contents/>:<ContentHistory/>
          }
        </div>
             
    )

}

export default connect(r=>r)(AvailabilityPage)