import React, { useEffect } from 'react'
import './main.css'
import HoverIcon from '../../parts/hoverIcon'
import { connect } from 'react-redux'
import { clickCard, currentPage, mouseIsOut } from '../../redux/actions'
import { IconButton } from '@material-ui/core'
import { Link } from 'react-router-dom'
 
function Main(props){
    
    return (
        <>
        <div className='cards'>
            {
                props.navs.navRow1.map((item,index)=>{
                    return(
                        <div className='card' key={item.title} onMouseLeave={()=>props.dispatch(mouseIsOut(index))}>
                            <img src={item.img}/>
                            <div className='caption'>
                                <div>
                                    <span>{item.title}</span>
                                    <span>{item.currency}</span>
                                </div>
                            </div>
                            <div className={item.show==false?'cardHover':'cardHoverAll'}  
                            onClick={()=>{props.dispatch(clickCard(index))}} 
                            >
                                <div className='information'>
                                    <div className='total'>
                                        <div className='left'>
                                            <span>наличие</span>
                                            <span>ровно</span>
                                        </div>
                                        <div className='right'>
                                            <span>{item.ok}</span>
                                        </div>
                                    </div>
                                    <div className='otherCurrencies'>
                                        <div className='left'>
                                            <span>eur 0.000</span>
                                            <span>rub 0.000</span>
                                            <span>xau 0.000</span>
                                            <span>xag 0.000</span>
                                        </div>
                                        <div className='right'>
                                            <Link to={item.route}>
                                                <IconButton>
                                                    <HoverIcon/>  
                                                </IconButton>  
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
            
        </div>
        </>
    )
}

export default connect(r=>r)(Main)