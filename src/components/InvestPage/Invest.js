import React, { useEffect } from 'react';
import SmallCards from '../../parts/smallCards';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { currentPage } from '../../redux/actions';
import { connect } from 'react-redux';
import invIcon from '../images/icon/investIcon.png'
import gr1 from '../images/icon/Group1.png'
import gr2 from '../images/icon/Group2.png'
import investIcon from '../images/icon/investIcon.png'
import investUp from '../images/icon/investUp.png'
import investDown from '../images/icon/investDown.png'
import './invest.css'
function Invest (props){
    useEffect(()=>{
        props.dispatch(currentPage(props.history.location.pathname))        
    },[])
    return(
        <>
        <SmallCards/>
        <div className='content'>
            <div className='goBack'>
                <a href='/'>
                    <ArrowBackIcon/>
                </a>
                       
            </div>
            <div className='icons'>
             

                
                </div>             
        </div>
        <div className='maincontent'>
            <div className='invicon'>
                <img src={invIcon}/>
            </div>
            <div className='investCards'>
                <div className='left'>
                    <h3>Name</h3>
                    <span>Вложено: 500 USD</span>
                    <span>Возврат: 600</span>
                    <span>Доход: 100</span>
                </div>
                <div className='right'>
                    <div className='invIcons'><img src={gr1}/></div>
                    <div className='invIcons'><img src={gr2}/></div>
                    <div className='invIcons'><img src={investUp}/></div>
                </div>
            </div>

            <div className='investCards'>
                <div className='left'>
                    <h3>Name</h3>
                    <span>Вложено: 500 USD</span>
                    <span>Возврат: 600</span>
                    <span>Доход: 100</span>
                </div>
                <div className='right'>
                    <div className='invIcons'><img src={gr1}/></div>
                    <div className='invIcons'><img src={gr2}/></div>
                    <div className='invIcons'><img src={investDown}/></div>
                </div>
            </div>
        </div>

        </>

    )

}

export default connect(r=>r)(Invest)