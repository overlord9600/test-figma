import React, { useEffect } from 'react';
import SmallCards from '../../parts/smallCards';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { currentPage } from '../../redux/actions';
import { connect } from 'react-redux';
function OrgonaizerPage (props){
    useEffect(()=>{
        props.dispatch(currentPage(props.history.location.pathname))        
    },[])
    return(
        <>
        <SmallCards/>
        <div className='content'>
            <div className='goBack'>
                <a href='/'>
                    <ArrowBackIcon/>
                </a>
                       
            </div>
            <div className='icons'>
             

                
                </div>             
        </div>

        </>

    )

}

export default connect(r=>r)(OrgonaizerPage)