import { IconButton } from '@material-ui/core';
import React from 'react';
import { connect } from 'react-redux';
import dots from '../components/images/icon/dots.png'
import lastIcon from '../components/images/icon/lastIcon.png'
import moreCoins from '../components/images/moreCoins.png'
import { createNewCategory, openToAdd } from '../redux/actions';
import NewCategory from './createNewCategory/newCategory';
function Contents (props){
    let items = props.navs.currentPage
    return(
        <>
        <div className='mainContent'>
                        <div className='card'>
                            <img src={items.img}/>
                            <div className='caption'>
                                <div>
                                    <span>{items.title}</span>
                                    <span>{items.currency}</span>
                                </div>
                            </div>
                        </div>
                <div className='currencies'>
                    <span>0.0000 usd</span>
                    <span>0.0000 eur</span>
                    <span>0.0000 rub</span>
                </div>
            </div>
        <div className='description'>
            {
                props.navs.categories.map((item,index)=>{
                    return(
                        <div key={index} className='largeCard' onClick={()=>{props.dispatch(openToAdd(item))}}>
                           <div className='largeCardContent'>
                           <div className='cardImage'>
                                <img src={moreCoins}/>
                            </div>
                            <div className='largeCardInfo'>
                                <span>{item.descrip}</span>
                                <span>{item.currency} USD</span>
                            </div>
                           </div>
                            <div className='cardIcon'>
                                <IconButton>
                                    <img src={dots}/>
                                </IconButton>
                            </div>
                        </div>
                    )
                })
            }
            <div className='lastIcon'>
                <IconButton onClick={()=>{props.dispatch(createNewCategory())}}>
                    <img src={lastIcon} />
                </IconButton>
            </div>
        </div>
       
        </>
    )

}

export default connect(r=>r)(Contents)