import React from 'react';
import Logo from './logo';
import './appbar.css'
import FilterIcon from './filter';
import { IconButton } from '@material-ui/core';
 
export default function Header(){
  return(
    <header className='header'>
      <div className='logo'>
        <Logo/>
      </div>
      <div className='searchForm'>
        <input type='search' placeholder='  Пишите здесь...' />
        <input type="submit" value="Поиск"/>
      </div>
      <div className='filterIcon'>
        <IconButton>
          <FilterIcon/>
        </IconButton>
      </div>
    </header>
  )
}