import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addToCategory, changeNewCategory2, closeModal2, pushNewCategory } from '../../redux/actions';
 
const  AddCategory= (props) => {
    return (
        <div className={props.navs.categoryModalOpen2==true?'bgLayer':'closedModal'}>
        <div className='modal'>
            <div className='modalHead'>
                <div className='modalTitle'><p>Приход</p> {props.navs.addCategoryValueError}</div>
                <div className='closeModal' onClick={()=>{props.dispatch(closeModal2())}}>&times;</div>
            </div>
            <div className='modalContent'>
                <div className='leftSide'>
                    <div className='textfeiled'>
                        Сумма
                            <input value={props.navs.addCategoryValue2.currency} onChange={(e)=>{props.dispatch(changeNewCategory2('currency',e.target.value))}}/>
                      
                    </div>
                    <div className='textfeiled'>
                          Описание
                        <textarea value={props.navs.addCategoryValue2.descrip} onChange={(e)=>{props.dispatch(changeNewCategory2('descrip',e.target.value))}}></textarea>
                    </div>
                </div>
                <div className='rightSide'>
                    <div className='file'>
                        <label  htmlFor='file' >
                            <input id='file' type='file'/>
                        </label>
                    </div>
                    <div className='flagIcons'>

                    </div>
                    <div className='btn'>
                        <button className='add' onClick={()=>props.dispatch(addToCategory())}>Добавить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}
 
export default connect(r=>r)(AddCategory);