import React from 'react';

export default function AnaliticBlock (){

    return(
        <div className='analitica'>
             <div className='analiticCard'>
                <div className='info'> 
                  <span>0.0000 usd</span> 
                </div>
                <div className='info'>
                  <p>0.0000 AMD</p>
                  <p>0.0000 EUR</p>
                  <p>0.0000 RUB</p>
                </div>
                <div className='info'> 
                  <p>0.0000 GOLD</p>
                  <p>0.0000 SILVER</p>
                </div>
             </div>
           </div>
    )

}