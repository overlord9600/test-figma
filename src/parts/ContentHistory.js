import React from 'react';
import './contentHistory.css'
import date from '../components/images/icon/date.png'
import { connect } from 'react-redux';

 function ContentHistory (props){
   return(
        <div className='showHistory'>
            <div className='filter'>
            <div className="sd-container">
    <input className="sd" type="date" name="selected_date" />
    <span className="open-button">
      <button type="button"><img src={date}/></button>
    </span>
  </div>
  <div className="sd-container">
    <input className="sd" type="date" name="selected_date" />
    <span className="open-button">
      <button type="button"><img src={date}/></button>
    </span>
  </div>
                <select>
                    <option>выберите категорию</option>
                </select>
                <select>
                    <option>валюта</option>
                </select>
            </div>
            <div className='dateInfo'>
                <div><span>2020</span></div>
                <div><span>nov</span></div>
            </div>
            <table>
                <thead>
                <tr>
                    <th>Дата и время</th>
                    <th>Сумма</th>
                    <th>Тип</th>
                    <th>Категория</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tbody>
                  {
                    props.navs.historyTable.map((item,index)=>{
                      return(
                        <tr key={index}>
                          <td>{new Date().getDate()}/{new Date().getMonth() + 1}/{new Date().getFullYear()} {new Date().getHours()}:{new Date().getMinutes()}</td>
                          <td>{item.currency}  USD</td>
                          <td>Приход</td>
                          <td>{item.category}</td>
                          <td>{item.descrip}</td>
                        </tr>
                      )
                    })
                  }
                </tbody>
            </table>
        </div>
    )

}

export default connect(r=>r)(ContentHistory)