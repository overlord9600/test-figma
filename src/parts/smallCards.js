import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './smallcard.css'
function SmallCard (props){

    return(
        <>
        <div className='cards'>
        {
             props.navs.navRow2.map((item,index)=>{
                 return(
                    <Link to={item.id}  key={item.id}>
                        <div className='smallCard'>
                        <img src={item.img}/>
                        <div className='caption'>
                            <div>
                                <span>{item.title}</span>
                                <span>{item.currency}</span>
                            </div>
                        </div>
                    </div>
                    </Link>
                 )
                })
        }
    </div>  
    
    </>    
    )

}

export default connect(r=>r)(SmallCard)